#encoding: utf-8
require 'rubygems'
require 'sinatra'
require 'sinatra/reloader'
require 'pry'
require 'sqlite3'
require 'sinatra/activerecord'

set :database, { adapter: 'sqlite3', database: 'brshop.db' }

class Client < ActiveRecord::Base
	#validates_presence_of :name, :email, :phone, :color, :barber, :datestamp
	validates :name, presence: true, length: {minimum:3}
	validates :email, presence: true
	validates :datestamp, presence: true
	validates :color, presence: true
 	validates :barber, presence: true
	validates :phone, presence: true
end

class Barber < ActiveRecord::Base
end

class Contact < ActiveRecord::Base
end

before do
	@barbers = Barber.all
end
get '/' do

	erb :index
end


get '/visit' do
	@c = Client.new
	erb :visit
end

post '/visit' do


	@datestamp = params[:user_date] +' '+ params[:user_time]
	@new_client = params.except("user_date","user_time").merge("datestamp"=>@datestamp)




	#@name = params[:name]
	#@email = params[:email]
	#@phone = params[:phone]
	#@user_date = params[:user_date]
	#@user_time = params[:user_time]
	#@barber = params[:barber]
	#@color = params[:color]
	@message = "Dear #{@new_client['name']} you are booked at #{@datestamp}.We will remind you 2 hours before the haircut"

	@c = Client.new(@new_client
				# name: @new_client['name'],
				# email: @new_client['email'],
				# phone: @new_client['phone'],
				# datestamp: @new_client['datestamp'],
				# barber: @new_client['barber'],
				# color: @new_client['color']
			)

		# c.name = @name
		# c.email = @email
		# c.datestamp = @datestamp
		# c.barber = @barber
		# c.color = @color
		# c.phone = @phone

		if @c.save
			erb "<h2> Thank you, we`ll remind you about your haircut!</h2>"
		else
			@error = @c.errors.full_messages.first
			erb :visit
		end

	#erb :message
end


get '/contacts' do
	erb :contacts
end

post '/contacts' do

	@user_text = params[:user_text]
	@name = params[:name]
	@mail = params[:mail]

	hh = {:name => 'Enter your name!',
		    :mail => 'Enter your mail!',
		    :user_text => 'Type text to contact with us!',

	}


	@error = hh.select {|key,_| params[key] == ""}.values.join(", ")
	if @error != ''
		return erb :contacts
	else 	cont = Contact.new name: "#{@name}", mail: "#{@mail}", message: "#{@user_text}"
		cont.save
		return erb :contacts
	end
end

get '/barber/:id' do
	@barber = Barber.find(params[:id])
	erb :barber
end

get '/bookings' do
	@clients = Client.order('created_at DESC')
	erb :bookings
end

get '/client/:id' do
	@client = Client.find(params[:id])
	erb :clients
end
